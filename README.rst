``python-nvm`` - Python implementation of the Nuggie Coin Virtual Machine
================================================================

Python implementation of Nuggie Coin Virtual Machine

The NVM is responsible for storing the Nuggie Coin state vector(s), processing transactions,
and running smart contracts (in the future,) and updating state vectors as required.