# -*- coding: utf-8 -*-
"""Module handles loading chain information, including genesis
"""

import json
from pathlib import Path


__all__ = ['ChainData']

from ..rlp import BlockHeader
from ..rlp.structs import RLPBlock


def get_json_data(chain):
    base = Path(__file__).parent
    full = base/chain/"chain.json"
    if not full.exists():
        raise ValueError(f"Chain '{chain}' does not exist!")
    with open(full, 'r') as f:
        return json.load(f)


class ChainData:
    def __init__(self, chain):
        self.chain = chain
        self.data = get_json_data(chain)

    @property
    def state_trie_root(self):
        return bytes.fromhex(self.data['state_trie']['root_hash'])

    @property
    def state_trie_db(self):
        in_hex = self.data['state_trie']['db']
        return {bytes.fromhex(i): bytes.fromhex(in_hex[i]) for i in in_hex}

    @property
    def genesis_block_header(self) -> BlockHeader:
        needs_bytes = ['parentHash', 'beneficiary', 'stateRoot', 'transactionsRoot', 'receiptsRoot']
        raw_dict = self.data['gen_block']['header']
        as_dict = {k: bytes.fromhex(raw_dict[k]) if k in needs_bytes else raw_dict[k] for k
                   in raw_dict}
        return BlockHeader(**as_dict)

    @property
    def genesis_block(self) -> RLPBlock:
        return RLPBlock(0, self.genesis_block_header, self.data['gen_block']['transactions'],
                        self.data['gen_block']['receipts'])
