# -*- coding: utf-8 -*-
"""Module handles blocks
"""

from typing import TYPE_CHECKING, Union

import time
import random

from eth_bloom import BloomFilter
from rlp import encode
from trie import HexaryTrie

from .._constants import *
from ..consensus.pow import verify_block
from ..rlp.structs import BlockHeader, Transaction, Receipt, LocalBlockHeader, RLPBlock

if TYPE_CHECKING:
    from ..vm.vm_state import LocalVMState, VMState
from .._utils import get_difficulty


__all__ = ['Block']


class Block:
    def __init__(self, parent, beneficiary, state, difficulty, height):
        if get_difficulty(difficulty) > get_difficulty(DIFF_TARGET):
            raise ValueError("Block difficulty target too high")
        self.final = False
        self.parent = parent
        self.beneficiary = beneficiary
        self.fee = 0
        self.state: 'LocalVMState' = state
        self.difficulty = get_difficulty(difficulty)
        self.raw_difficulty = difficulty
        self.height = height
        self.timestamp = int(time.time())
        self.transactions = HexaryTrie({})
        self._transactions = []
        self.transaction_count = 0
        self.receipts = HexaryTrie({})
        self._receipts = []
        self.logs_bloom = BloomFilter()
        self._header = LocalBlockHeader(self.parent, self.beneficiary, self.state.accounts.root_hash,
                                        self.transactions.root_hash, self.receipts.root_hash,
                                        int(self.logs_bloom), self.raw_difficulty, self.height,
                                        0, 0, self.timestamp, random.getrandbits(64), 0)

    @property
    def hash(self):
        return self.header.hash

    @property
    def mined(self):
        return verify_block(self._header.to_rlp())

    @property
    def header(self):
        return self._header.rlp

    def _put_transaction(self, transaction: Transaction):
        idx = self.transaction_count.to_bytes((self.transaction_count.bit_length() % 8) + 1, 'big')
        self.transactions[idx] = encode(transaction)

    def _put_receipt(self, receipt: Receipt):
        idx = self.transaction_count.to_bytes((self.transaction_count.bit_length() % 8) + 1, 'big')
        self.receipts[idx] = encode(receipt)

    def _add_t_and_r(self, transaction: Transaction, receipt: Receipt):
        self._put_transaction(transaction)
        self._transactions.append(transaction)
        self.logs_bloom |= BloomFilter(receipt.bloom)
        self._header.gasUsed = receipt.cumulativeGasUsed
        self._header.gasLimit += transaction.gasLimit
        self.fee += transaction.gasLimit * transaction.gasPrice
        self._put_receipt(receipt)
        self._receipts.append(receipt)
        self.transaction_count += 1
        self._update_roots()

    def _update_roots(self):
        self._header.transactionsRoot = self.transactions.root_hash
        self._header.receiptsRoot = self.receipts.root_hash

    def _mine(self, tries):
        if self.final:
            raise ValueError("Block final")
        i = 0
        while i < tries:
            self._header.nonce += 1
            if self._header.nonce > 0xFFFFFFFF:
                self._header.nonce = 0
                self._header.randomNonce = random.getrandbits(64)
            if self.mined:
                return True, i
            i += 1
        return False, i

    def mine(self, tries=10000):
        tic = time.time()
        result, attempts = self._mine(tries)
        toc = time.time()
        hashrate = attempts / (toc-tic)
        return result, hashrate, attempts

    def add_transaction(self, transaction: Transaction):
        if self.final:
            raise ValueError("Block final")
        from python_nvm.consensus.check_transaction import check_bavt
        status = check_bavt(transaction, self.state)
        if status == ERR_BAD_SIG:
            # Signature failed to verify
            return False, ERR_BAD_SIG
        elif status == ERR_CONTRACT_ACCOUNT:
            # This is a contract account
            return False, ERR_CONTRACT_ACCOUNT
        elif status == ERR_BAD_NONCE:
            return False, ERR_BAD_NONCE
        elif status == ERR_OUT_OF_GAS:
            # Include, and take all the gas
            receipt = Receipt(self._header.gasUsed + transaction.gasLimit, int(BloomFilter()), [],
                              OUT_OF_GAS)
            self._add_t_and_r(transaction, receipt)
            return True, OUT_OF_GAS
