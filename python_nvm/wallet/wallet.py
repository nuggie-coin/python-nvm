# -*- coding: utf-8 -*-
"""Module handles wallets and related functions.
"""
import rlp
from mnemonic import Mnemonic
from eth_keys import keys
from eth_hash.auto import keccak
import json

from .._constants import *
from .._utils import addr_checksum_encode


__all__ = ['generate_phrase', 'Wallet']

from ..rlp import ExternalAccount, Transaction


def generate_phrase(lang="english"):
    mnemo = Mnemonic(lang)
    return mnemo.generate(256)


class Wallet:
    def __init__(self, seed):
        if len(seed) != 32:
            raise ValueError("Key seed must be 32 bytes")
        self.seed = seed

    def get_sk(self, index=0, use='ECA-ADDR'):
        seed_hash = keccak(self.seed)
        index_hash = keccak(f"{use}/{index}".encode('ascii'))
        return keys.PrivateKey(keccak(seed_hash + index_hash))

    def get_pk(self, index=0, use='ECA-ADDR'):
        return keys.private_key_to_public_key(self.get_sk(index=index, use=use))

    def get_addr(self, index=0):
        return keccak(self.get_pk(index=index, use='ECA-ADDR').to_bytes())[-28:]

    def get_str_addr(self, index=0):
        return addr_checksum_encode(self.get_addr(index=index))

    @classmethod
    def from_phrase(cls, phrase, password='', lang='english'):
        mnemo = Mnemonic(lang)
        seed = keccak(mnemo.to_seed(phrase, password))
        return cls(seed)

    def to_dict(self, password=''):
        data = {'wallet-version': 0}
        key = keccak(password.encode('ascii'))
        enc_seed = bytes([self.seed[i] ^ key[i] for i in range(32)])
        data['seed'] = enc_seed.hex()
        data['seed-check'] = keccak(self.seed).hex()
        return data

    @classmethod
    def from_dict(cls, data, password=''):
        if data['wallet-version'] != 0:
            raise ValueError("Wallet version incorrect")
        key = keccak(password.encode('ascii'))
        seed = bytes([bytes.fromhex(data['seed'])[i] ^ key[i] for i in range(32)])
        if keccak(seed).hex() != data['seed-check']:
            raise ValueError("Seed check did not match: password incorrect")
        return cls(seed)

    def to_file(self, filename, password=''):
        with open(filename, 'w') as f:
            json.dump(self.to_dict(password=password), f)

    @classmethod
    def from_file(cls, filename, password=''):
        with open(filename, 'r') as f:
            data = json.load(f)
        return cls.from_dict(data, password=password)

    def send_bavt_to(self, to: bytes, value: int, gas_price: int, from_acc: ExternalAccount,
                     index=0):
        if len(to) != 28:
            raise ValueError("Address length incorrect")
        fee = BAVT_GAS_COST * gas_price
        if (fee+value) > from_acc.balance:
            raise ValueError("Account balance insufficient")
        to_sign = Transaction(from_acc.nonce, gas_price, BAVT_GAS_COST, to, value, b'', 0, 0, 0)
        sig = self.get_sk(index, use='ECA-ADDR').sign_msg(rlp.encode(to_sign))
        trans = Transaction(from_acc.nonce, gas_price, BAVT_GAS_COST, to, value, b'',
                            sig.v, sig.r, sig.s)
        return trans
