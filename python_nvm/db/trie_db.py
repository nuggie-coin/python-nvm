# -*- coding: utf-8 -*-
"""Module handles generic db-backed trie storage for rlp serializable/byte objects

Values are stored in a hexary trie locally, and are written to disk in a sqlite3 database.

Database can be accessed as a dictionary.  When a write is made, it is cached and will not be
written to disk until the commit() method is called.  Alternatively, the revert() method can be called
"""

import sqlite3
from trie import HexaryTrie
from pathlib import Path
import rlp
from typing import Union


__all__ = ['LocalTrieDB', 'TrieDB']


class LocalTrieDB:
    def __init__(self):
        self._trie: HexaryTrie = HexaryTrie({})
        self._changeList = []
        self._committed = []

    @property
    def root_hash(self):
        return self._trie.root_hash

    def __getitem__(self, item: bytes) -> Union[rlp.Serializable, bytes]:
        return self.get(item)

    def __setitem__(self, key: bytes, value: Union[rlp.Serializable, bytes]):
        self.write(key, value)

    def _get_proc(self, key: bytes, value: bytes) -> Union[rlp.Serializable, bytes]:
        return value

    def get(self, key: bytes) -> Union[rlp.Serializable, bytes]:
        if key in self._trie:
            return self._get_proc(key, self._trie[key])
        raise KeyError(f"Key {key} not in trie")

    def _write_proc(self, key: bytes, value: Union[rlp.Serializable, bytes]) -> bytes:
        return value

    def write(self, key: bytes, value: Union[rlp.Serializable, bytes]):
        value = self._write_proc(key, value)
        if key in self._trie:
            # Update
            self._changeList.append((key, self._trie[key], value))
        else:
            # New account
            self._changeList.append((key, None, value))
        self._trie[key] = value

    def commit(self):
        for i in self._changeList:
            self._committed.append(i)

    def revert(self):
        for change in reversed(self._changeList):
            if change[1] is not None:
                self._trie[change[0]] = change[1]
            else:
                del self._trie[change[0]]
        self._changeList = []


class TrieDB(LocalTrieDB):
    def __init__(self, dbfile: Union[Path, str]):
        super().__init__()
        if isinstance(dbfile, str):
            self._fname: Path = Path(dbfile)
        elif isinstance(dbfile, Path):
            self._fname: Path = dbfile
        if not self._fname.exists():
            tmpdb = sqlite3.connect(self._fname)
            tmpcur = tmpdb.cursor()
            tmpcur.execute("create table vals (key blob, value blob);")
            tmpdb.commit()
            tmpcur.close()
        self._conn: sqlite3.Connection = sqlite3.connect(self._fname)
        self._load_db()

    def _load_db(self):
        cur = self._conn.cursor()
        cur.execute("select key, value from vals;")
        rows = cur.fetchmany()
        while rows:
            for row in rows:
                self._trie[row[0]] = row[1]
            rows = cur.fetchmany()
        cur.close()

    def commit(self):
        cur = self._conn.cursor()
        for change in self._changeList:
            if change[1] is not None:
                cur.execute("UPDATE vals SET value=? WHERE key=?;",
                            (change[2], change[0]))
            else:
                cur.execute("INSERT INTO vals(key, value) VALUES(?,?);",
                            (change[0], change[2]))
        self._conn.commit()
        cur.close()
        self._changeList = []
