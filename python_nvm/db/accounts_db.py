# -*- coding: utf-8 -*-
"""Module handles storage for accounts.

There are two types of accounts in the Nuggie Coin system - contract accounts and externally
owned accounts.

Contract accounts contain a contract that controls their funds.  Four values are stored:
    [nonce, balance, storageRoot, codeHash]

Externally owned accounts are controlled by an external user that has access to the private key.
Two values are stored:
    [nonce, balance]

Within the account trie/db, the difference is implicitly stored in the length of the list of account
values in the RLP account.
"""

from rlp import encode, decode
from pathlib import Path
from typing import Union

from trie import HexaryTrie

from ..rlp.structs import ContractAccount, ExternalAccount
from .trie_db import TrieDB, LocalTrieDB

__all__ = ['AccountsDB', 'LocalAccountsDB']


class AccountDBBase:
    def _get_proc(self, key: bytes, value: bytes) -> Union[ContractAccount, ExternalAccount]:
        if len(key) != 28:
            raise ValueError(f"Malformed address")
        acc = decode(value)
        if len(acc) == 2:
            return decode(value, ExternalAccount)
        elif len(acc) == 4:
            return decode(value, ContractAccount)
        raise ValueError(f"Address 0x{key.hex()} does not contain a valid account")

    def _write_proc(self, key: bytes, value: Union[ContractAccount, ExternalAccount]) -> bytes:
        if len(key) != 28:
            raise ValueError(f"Malformed address")
        value = encode(value)
        return value


class AccountsDB(AccountDBBase, TrieDB):
    def __init__(self, dbfile: Union[Path, str]):
        super().__init__(dbfile)


class LocalAccountsDB(AccountDBBase, LocalTrieDB):
    @classmethod
    def from_accounts_db(cls, db: AccountsDB):
        x = cls()
        x._trie = HexaryTrie(db._trie.db, root_hash=db.root_hash)
        return x
