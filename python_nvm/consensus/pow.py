# -*- coding: utf-8 -*-
"""Module handles proof of work functions
"""


from eth_hash.auto import keccak
from .._utils import get_difficulty, to_difficulty
from ..rlp.structs import BlockHeader


__all__ = ['verify_block']


def verify_block(header: BlockHeader) -> bool:
    diff = get_difficulty(header.difficulty)
    return int.from_bytes(header.hash, 'big') <= diff
