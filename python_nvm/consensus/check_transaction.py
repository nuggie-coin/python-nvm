# -*- coding: utf-8 -*-
"""Module handles validating transactions actually work
"""
from typing import Union, TYPE_CHECKING

from .._utils import verify_and_addr
from .._constants import *
from ..rlp.structs import Transaction, ContractAccount, ExternalAccount
if TYPE_CHECKING:
    from ..vm.vm_state import LocalVMState

__all__ = ['check_bavt', 'check_nonce']


def check_bavt(transaction: Transaction, state: 'LocalVMState') -> int:
    good, addr = verify_and_addr(transaction)
    if not good:
        return ERR_BAD_SIG
    acc = state.accounts[addr]
    if isinstance(acc, ContractAccount):
        return ERR_CONTRACT_ACCOUNT
    if not check_nonce(transaction, acc):
        return ERR_BAD_NONCE
    if transaction.gasLimit < BAVT_GAS_COST:
        return ERR_OUT_OF_GAS
    less_gas = get_less_gas(transaction, acc)
    if less_gas <= transaction.value:
        return ERR_OUT_OF_FUNDS
    return OK


def check_nonce(transaction: Transaction, account: ExternalAccount) -> bool:
    return transaction.nonce == account.nonce


def get_less_gas(transaction: Transaction, account: Union[ExternalAccount, ContractAccount]) \
        -> int:
    bal = account.balance
    bal -= (transaction.gasLimit * transaction.gasPrice)
    return bal
