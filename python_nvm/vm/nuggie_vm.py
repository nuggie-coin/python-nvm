# -*- coding: utf-8 -*-
"""Module handles main nuggie VM
"""
from typing import Tuple

import ecdsa
import rlp
from eth_hash.auto import keccak
from eth_bloom import BloomFilter

from .._constants import TRANSACTION_OK, REVERTED, OUT_OF_GAS, NOT_INCLUDED, OUT_OF_FUNDS
from .vm_state import VMState
from ..rlp.structs import Transaction, Receipt, ExternalAccount, ContractAccount


__all__ = []



