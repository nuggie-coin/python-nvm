# -*- coding: utf-8 -*-
"""Module handles VM state class
"""

from pathlib import Path
from typing import Union

import rlp

from .. import EMPTY_TRIE_ROOT
from ..chain import ChainData
from ..db.accounts_db import AccountsDB, LocalAccountsDB
from ..rlp import ExternalAccount


__all__ = ['VMState', 'LocalVMState']



class VMState:
    def __init__(self, acc_file: Union[Path, str]):
        self.accounts = AccountsDB(acc_file)

    @classmethod
    def from_genesis(cls, acc_file: Union[Path, str], net: str):
        state = cls(acc_file)
        if state.accounts.root_hash != EMPTY_TRIE_ROOT:
            raise ValueError("On disk DB not empty!")
        net_data = ChainData(net)
        for k in net_data.state_trie_db:
            state.accounts.write(k, rlp.decode(net_data.state_trie_db[k], ExternalAccount))
        if state.accounts.root_hash != net_data.state_trie_root:
            raise ValueError(f"Account trie root mismatch.  Genesis: "
                             f"{net_data.state_trie_root.hex()}, Built: "
                             f"{state.accounts.root_hash.hex()}")
        state.accounts.commit()
        return state


class LocalVMState:
    def __init__(self):
        self.accounts = LocalAccountsDB()

    @classmethod
    def from_state(cls, state: VMState):
        x = cls()
        x.accounts = LocalAccountsDB.from_accounts_db(state.accounts)
        return x
