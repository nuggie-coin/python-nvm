# -*- coding: utf-8 -*-
"""Module creates rlp base sedes types
"""

from rlp.sedes import BigEndianInt, Binary

__all__ = ['address', 'hash32', 'trie_root', 'uint4', 'uint8', 'sig_v', 'sig_rs',
           'bloomFilter']

address = Binary.fixed_length(28, allow_empty=True)
hash32 = Binary.fixed_length(32)
trie_root = Binary.fixed_length(32, allow_empty=True)
uint8 = BigEndianInt(8)
uint4 = BigEndianInt(4)
sig_v = BigEndianInt(1)
sig_rs = BigEndianInt(32)
bloomFilter = BigEndianInt(256)
