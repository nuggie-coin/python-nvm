# -*- coding: utf-8 -*-
"""Module creates rlp serializable structs
"""

from collections import namedtuple
from typing import Union

from eth_hash.auto import keccak
import rlp
import snappy
from rlp.sedes import big_endian_int, binary
from rlp.sedes.lists import List, CountableList
from .sedes import address, sig_v, sig_rs, hash32, trie_root, bloomFilter, uint4, uint8


__all__ = ['Transaction', 'Receipt', 'ContractAccount', 'ExternalAccount', 'BlockHeader',
           'LocalBlockHeader']


class LocalSerializable:
    fields = []
    _rlp_class = None

    def __init__(self, *args, **kwargs):
        super(LocalSerializable, self).__setattr__('_db', {})
        alen = len(super(LocalSerializable, self).__getattribute__('fields'))
        keys = list(super(LocalSerializable, self).__getattribute__('fields'))
        if len(args) == alen:
            for i in range(len(args)):
                super(LocalSerializable, self).__getattribute__('_db')[keys[i]] = args[i]
            return
        elif len(args) != 0:
            raise ValueError(f"{self.__class__.__name__} expects {alen} positional arguments but"
                             f" {len(args)} were given")
        for key in keys:
            super(LocalSerializable, self).__getattribute__('_db')[key] = kwargs.get(key, None)

    def __getattribute__(self, item):
        if item in super(LocalSerializable, self).__getattribute__('fields'):
            return super(LocalSerializable, self).__getattribute__('_db')[item]
        return super(LocalSerializable, self).__getattribute__(item)

    def __setattr__(self, key, value):
        if key in super().__getattribute__('fields'):
            super(LocalSerializable, self).__getattribute__('_db')[key] = value
        else:
            super(LocalSerializable, self).__setattr__(key, value)

    @classmethod
    def from_rlp(cls, serializable: rlp.Serializable):
        return cls(**serializable.as_dict())

    @property
    def rlp(self):
        return self.to_rlp()

    def to_rlp(self):
        if self._rlp_class is None:
            raise ValueError('RLP base class not set')
        return self._rlp_class(**self._db)


class Transaction(rlp.Serializable):
    fields = [
        ('nonce', big_endian_int),
        ('gasPrice', big_endian_int),
        ('gasLimit', big_endian_int),
        ('address', address),
        ('value', big_endian_int),
        ('message', binary),
        ('v', sig_v),
        ('r', sig_rs),
        ('s', sig_rs)
    ]

    @property
    def is_bavt(self):
        return self.message == b'' and self.address != b'\x00'*28 and self.address != b''


class Receipt(rlp.Serializable):
    fields = [
        ('cumulativeGasUsed', big_endian_int),
        ('bloom', big_endian_int),
        ('logs', List),
        ('status', binary)
    ]


class ContractAccount(rlp.Serializable):
    fields = [
        ('nonce', big_endian_int),
        ('balance', big_endian_int),
        ('storageRoot', trie_root),
        ('codeHash', hash32)
    ]


class ExternalAccount(rlp.Serializable):
    fields = [
        ('nonce', big_endian_int),
        ('balance', big_endian_int)
    ]


class BlockHeader(rlp.Serializable):
    fields = [
        ('parentHash', hash32),
        ('beneficiary', address),
        ('stateRoot', trie_root),
        ('transactionsRoot', trie_root),
        ('receiptsRoot', trie_root),
        ('logsBloom', big_endian_int),
        ('difficulty', uint4),
        ('height', big_endian_int),
        ('gasLimit', big_endian_int),
        ('gasUsed', big_endian_int),
        ('timestamp', uint8),
        ('randomNonce', uint8),
        ('nonce', uint4)
    ]

    @property
    def hash(self):
        return keccak(rlp.encode(self))


class LocalBlockHeader(LocalSerializable):
    fields = [
        'parentHash',
        'beneficiary',
        'stateRoot',
        'transactionsRoot',
        'receiptsRoot',
        'logsBloom',
        'difficulty',
        'height',
        'gasLimit',
        'gasUsed',
        'timestamp',
        'randomNonce',
        'nonce'
    ]

    _rlp_class = BlockHeader


class RLPBlock(rlp.Serializable):
    fields = [
        ('version', big_endian_int),
        ('header', BlockHeader),
        ('transactions', CountableList(Transaction)),
        ('receipts', CountableList(Receipt))
    ]
