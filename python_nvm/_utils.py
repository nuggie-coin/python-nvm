# -*- coding: utf-8 -*-
"""Module contains useful functions
"""
from typing import Tuple

from eth_hash.auto import keccak
from eth_keys import keys
import rlp

from .rlp.structs import Transaction


__all__ = ['verify_and_addr', 'to_difficulty', 'get_difficulty', 'addr_checksum_encode',
           'addr_check']


def verify_and_addr(trans: Transaction) -> Tuple[bool, bytes]:
    # Get non-signed copy
    ns = Transaction(trans.nonce, trans.gasPrice, trans.gasLimit, trans.address, trans.value,
                     trans.message, 0, 0, 0)
    message = rlp.encode(ns)
    sig = keys.Signature(vrs=(trans.v, trans.r, trans.s))
    pk = sig.recover_public_key_from_msg(message)
    addr = keccak(pk.to_bytes())[-28:]
    return sig.verify_msg(message, pk), addr


def get_difficulty(val):
    """Expand compact difficulty into full 256-bit number.

    Compact difficultly acts as a type of floating point number.  The first byte is an exponent,
    and the last three are the mantissa.

    :param val: Compact difficulty

    :returns: Full difficulty
    """
    exp = (val & 0xFF000000) >> 24
    mant = val & 0xFFFFFF
    return (mant << exp) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF


def to_difficulty(val):
    if val > 65133043294244013135194891632631085890438913923313845101808335841926911623168:
        return int('ffffffff', 16)
    bin_str = bin(val)[2:]
    l = len(bin_str)
    if l <= 24:
        return val
    mant = int(bin_str[0:24], 2)
    exp = l-24
    return (exp << 24) | mant


def get_hashrate(diff: int) -> float:
    base = get_difficulty(diff)
    max = (2**256)-1
    hashes = max/base
    return hashes / 60.


def addr_checksum_encode(addr: bytes) -> str:
    if len(addr) != 28:
        raise ValueError("Address length incorrect")
    hex_addr = addr.hex()
    hashed_addr = keccak(hex_addr.encode('ascii')).hex()
    check_addr = ''
    for idx, char in enumerate(hex_addr):
        if char in '0123456789':
            check_addr += char
        elif char in 'abcdef':
            nibble = int(hashed_addr[idx], 16)
            if nibble > 7:
                check_addr += char.upper()
            else:
                check_addr += char
        else:
            raise ValueError("Not a hex char in in hex string!")
    return "NGx" + check_addr


def addr_check(addr: str) -> bool:
    if not addr.startswith("NGx"):
        return False
    byte_addr = bytes.fromhex(addr[3:])
    return addr == addr_checksum_encode(byte_addr)
