#!/usr/bin/env python
from setuptools import setup
from os import path
import sys

here = path.abspath(path.dirname(__file__))

if 'upload' in sys.argv or 'twine' in sys.argv:
    raise Exception("Library not intended for upload at this time")

def readme():
    with open('README.rst') as f:
        return f.read()

setup(
    name='python_nvm',
    version='0.0.1',
    description='Python Implementation of the Nuggie Coin Virtual Machine',
    long_description=readme(),
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    keywords='nugge thenuggie cryptocurrency',
    url='https://gitlab.com/nuggie-coin/python-nvm',
    author='Robert Frazee',
    author_email='robert@rfrazee.com',
    license='MIT',
    packages=['python_nvm'],
    include_package_data=True,
    install_requires=['ecdsa',
                      'base58',
                      'pyzmq',
                      'rlp',
                      'trie',
                      'lru-dict',
                      'python-snappy',
                      'eth-bloom',
                      'eth-keys',
                      'mnemonic']
)
